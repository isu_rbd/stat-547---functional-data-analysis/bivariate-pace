---
title: "STAT 547 | HW 5"
author: "Ricardo Batista"
date: "11/9/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse); library(readr); library(reshape2); library(fdapace)
library(roxygen2); library(locfit)
```

# Question 1

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}
# General assumptions
M  <- 100
T <- seq(0, 1, length.out = M)
mu <- (T)^2*cos((3/2)*T)
n  <- 50

ggplot(data.frame(t = T, mu), aes(t, mu)) + theme_bw() + 
  labs(title = "mu") + ylim(c(0, 0.4)) +
  geom_line(show.legend = FALSE)

```

## Scenarios

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}
# The two options for curves:

# lambda = seq(0.0001, 0.00001, length.out = K)
# lambda = exp(-(1:K))

# run whole analysis
## plot
```

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}
get_dense <- function(K, sigma) {
  
  # plot the res
  # Return number of components by different methods
  # side-by-side plot of FPC scores for blup vs the other
  
}

get_sparse <- function(K, sigma) {
  
  
    
}

```

### Dense, K = 20, low noise

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}
K <- 20; sigma <- 0.01

res <- MakeGPFunctionalData(n, mu = mu, K = K,
                            lambda = exp(-(1:K)),
                            sigma  = 0.0001)

data <- data.frame(t = res$pts, y = t(res$Yn)) %>% 
  `colnames<-`(c("t", sprintf("y%d", 1:nrow(res$Y)))) %>%
  melt(variable.name = 'id', id.vars = 't', value.name = "y")

ggplot(data, aes(t, y, color = id)) + theme_bw() + 
  labs(title = "Sample (Dense, K = 20, low noise)") +
  geom_line(size = 0.01, show.legend = FALSE)

# Number of components chosen
samp <- MakeFPCAInputs(data$id, data$t, data$y)
res <- FPCA(samp$Ly, samp$Lt, optns = list(methodSelectK = "FVE", methodXi = "IN"))
# fpc <- data.frame(as.factor(1:n), res$xiEst[, 1:2]) %>% 
#   `colnames<-`(c("id", "PC1", "PC2")) # take top two FPCs
# ggplot(fpc, aes(PC1, PC2, color = id)) + geom_text(aes(label=id), size = 3) + 
#   theme_bw() + theme(legend.position = "none")
# 
# res <- FPCA(samp$Ly, samp$Lt, optns = list(methodSelectK = "FVE", methodXi = "CE"))
# K_FVE <- res$selectK
# res <- FPCA(samp$Ly, samp$Lt, optns = list(methodSelectK = "AIC"))
# K_AIC <- res$selectK
# res <- FPCA(samp$Ly, samp$Lt, optns = list(methodSelectK = "BIC"))
# K_BIC <- res$selectK


# FPC score comparison

## BLUP

## Integration

```

### Dense, K = 20, high noise

### Dense, K = 3, low noise

### Dense, K = 3, high noise

### Sparse, K = 20, low noise

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}

# MakeSparseGP

# Number of components chosen

# FPC score comparison

## BLUP

## True scores

```

### Sparse, K = 20, high noise

### Sparse, K = 3, low noise

### Sparse, K = 3, high noise


## (a)

&nbsp;&nbsp;&nbsp;&nbsp;

## (b)

## (c)

```{r echo=FALSE, fig.align = "center", fig.height=4, fig.width=6, message=FALSE, warning=FALSE}

```

# Question 2

&nbsp;&nbsp;&nbsp;&nbsp; We introduce the `biPACE` method, preceded by its documentation.

## PACE method for bivariate sparsely observed longitudial data

&nbsp;&nbsp;&nbsp;&nbsp; We explore the `biPACE` method via example. Suppose we would like to produce a best linear unbiased predictor (BLUP) for a bivariate response, in this case the weight and body length of Tammar wallabys.

```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}
data <- read.delim('http://www.statsci.org/data/oz/wallaby.txt') %>%
  select(Anim, Age, Weight, Leng) %>%
  `colnames<-`(c("id", "t", "y1", "y2")) %>%
  mutate(y1 = scale(y1), y2 = scale(y2)) %>%
  filter(t >= 322)

ggplot(data, aes(t, y1, color = as.factor(id))) + 
  labs(title = "Wallaby weight vs age (standardized)", y = "weight") +
  geom_line(size = 0.01) + geom_point(size = 0.5) +
  theme_bw() + theme(legend.position = "none") 

ggplot(data, aes(t, y2, color = as.factor(id))) + 
  labs(title = "Wallaby length vs age (standardized)", y = "length") +
  geom_line(size = 0.01) + geom_point(size = 0.5) +
  theme_bw() + theme(legend.position = "none") 
```

### Presmoothing

&nbsp;&nbsp;&nbsp;&nbsp; Since we are dealing with sparse data, our first step is to smooth each data set (independently):

```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}
# Smoothing parameters | General
ngrid <- 100; tGrid <- seq(min(data$t), max(data$t), length.out = ngrid) 
hmu <- c(75, 75)

## Y1
resMu1 <- locfit(y1 ~ lp(t, deg = 1, h = hmu[1]), data, 
                ev = lfgrid(mg = ngrid)) 

muHat1 <- predict(resMu1) 
ggplot() +
  labs(title = "Mu hat for weight", y = "weight") +
  geom_point(data = data, aes(t, y1), size = 0.75, alpha = 0.5) +
  geom_line(data = data.frame(t = tGrid, muHat = muHat1), aes(t, muHat), color = "red") +
  theme_bw() + coord_cartesian(ylim = range(data$y1, na.rm = TRUE))

## Y1
resMu2 <- locfit(y2 ~ lp(t, deg = 1, h = hmu[2]), data, 
                ev = lfgrid(mg = ngrid)) 

muHat2 <- predict(resMu2) 
ggplot() +
  labs(title = "Mu hat for length", y = "length") +
  geom_point(data = data, aes(t, y2), size = 0.75, alpha = 0.5) +
  geom_line(data = data.frame(t = tGrid, muHat = muHat2), aes(t, muHat), color = "red") +
  theme_bw() + coord_cartesian(ylim = range(data$y2, na.rm = TRUE))
```


### Covariance surface

```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}

# Get raw cov
## Need to somehow create a full matrix of values, i.e., I need to somewho produce a full set of values
## How to deal with NAs? Somehow produce an estimate for missing values given the observed points

# Consider foregoing covariance smoothing altogether --> just produce the covariance matrix as in HW2

# Get raw cov 
## Melt the data set
dat <- melt(data, id.vars = c('t', 'id'), value.name = "y")

## Center
muObs_1 <- predict(resMu1, data$t); yCenter_1 <- data$y1 - muObs_1
muObs_2 <- predict(resMu2, data$t); yCenter_2 <- data$y2 - muObs_2

## Stitch
#### Change t for y1 to just be a continuiation of the previous one --> this way I can get a surface with non-overlapping points
dat$yCenter <- c(yCenter_1, yCenter_2)

# Get rcov
rcov <- plyr::ddply(dat, 'id', function(d) {
  #browser() 
  raw <- c(tcrossprod(d$yCenter)) 
  TT <- expand.grid(t1 = d$t, t2 = d$t) 
  cbind(TT, raw = raw) 
}) 

hCov <- 100
# double the grid
resCov <- locfit(raw ~ lp(t1, t2, deg = 1, h = hCov), 
                 rcov %>% filter(t1 != t2),
                 ev = lfgrid(mg = ngrid), kern = 'epan') 

covMat <- matrix(predict(resCov), ngrid, ngrid) 


# Deal with corner with sparse observations? Can't do this in programatic fashion, though... 

plot3d(rcov$t1, rcov$t2, rcov$raw)

persp3d(tGrid, tGrid, covMat, add=FALSE, col='white')
# rgl.postscript("graph_covsurf.pdf","pdf")

```


```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}

eig <- eigen(covMat)
rmInd <- eig$values <= 0 
eig$values <- eig$values[!rmInd] 
eig$vectors <- eig$vectors[, !rmInd, drop=FALSE] 

lam <- eig$values * diff(range(tGrid)) / ngrid 
phi <- eig$vectors / sqrt(diff(range(tGrid)) / ngrid) 

# Split into two
phi_1  <- head(phi, ngrid/2)
phi_2  <- tail(phi, ngrid/2)

```


### Sigma^2

```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}

# Diagonal element
diagRes <- locfit(raw ~ lp(t1, deg = 1, h=hCov),  
                  rcov %>% filter(t1 == t2),  
                  ev=lfgrid(mg=ngrid)) 

Vhat <- predict(diagRes) 
sig2 <- mean(Vhat - diag(covMat))

lines3d(tGrid, tGrid, Vhat)
# rgl.postscript("graph_covsurf_wline.pdf","pdf")

```


### BLUP step

#### Y_1

```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}

dat_1 <- filter(dat, variable == "y1")

# BLUP step 
K <- 3 
LambdaK <- diag(lam[seq_len(K)], nrow=K) 
xi_1 <- plyr::ddply(dat_1, 'id', function(d) { 
  #browser() 
  # Linear interpolation 
  Phii <- matrix(fdapace::ConvertSupport(tGrid, d$t, 
                                         phi=phi_1[, seq_len(K), drop=FALSE]), 
                 ncol=K) 
  
  SigYi <- fdapace::ConvertSupport(tGrid, d$t, Cov=covMat) + diag(sig2, nrow=nrow(d)) 
  res <- c(LambdaK %*% t(Phii) %*% solve(SigYi, d$yCenter)) 
  names(res) <- paste0('xi', seq_len(K)) 
  res 
  
})

# plot(xi$xi1, xi$xi2) 

# X_K 
XK <- as.matrix(xi[, -1]) %*% t(phi[, seq_len(K), drop=FALSE]) + matrix(muHat, 
                                                                        nrow=nrow(xi), 
                                                                        ncol=ngrid,
                                                                        byrow=TRUE) 

plotInd <- c(2, 26) 
matplot(tGrid, t(XK[plotInd, ]), type='l', col = c("red", "blue")) 
lines(tGrid, muHat, lwd=3) 
points(dat$t[dat$id == plotInd[1]], dat$y[dat$id == plotInd[1]], cex=1, col='red') 
points(dat$t[dat$id == plotInd[2]], dat$y[dat$id == plotInd[2]], cex=1, col='blue') 

```


[BLUP by bivariate response]
- Create grids for both (see ConvertSupport for 2D in HW 4)



```{r echo=FALSE, fig.align = "center", fig.height=5, fig.width=10, message=FALSE, warning=FALSE}
#' biPACE
#' Allow user to submit hCov
#' Maybe even allow user to submit areas that should be excluded from cov surface
biPACE <- function(data, hmu, ngrid = 50) {
  
  # Data prep: rename columns and scale
  data <- data %>% `colnames<-`(c("id", "t", "y1", "y2")) %>%
    mutate(y1 = scale(y1), y2 = scale(y2))
  
  # Smooth each function independently
  
  ## f
  
  
  
}
```

