library(dplyr) 

library(ggplot2) 

library(locfit) 



dat <- read.delim('http://www.statsci.org/data/oz/wallaby.txt') %>%  
  
  select(Anim, Age, Leng) %>%  # select takes the columns 
  
  mutate(Age = Age / 365.24, Leng = Leng / 10) %>%  
  
  na.omit %>% 
  
  filter(Age >= 1 & Age <= 1.8) %>%  
  
  dplyr::rename(t = Age, y = Leng, id = Anim) 



ngrid <- 50 

hmu <- 0.1 

plot(dat$t, dat$y) 

resMu <- locfit(y ~ lp(t, deg=1, h=hmu), dat, ev=lfgrid(mg=ngrid)) 

tGrid <- seq(min(dat$t), max(dat$t), length.out=ngrid) 

muHat <- predict(resMu) 

lines(tGrid, muHat) 



# Get raw cov 

muObs <- predict(resMu, dat$t) # interpolate 

dat$yCenter <- dat$y - muObs 



rcov <- plyr::ddply(dat, 'id', function(d) { 
  
  #browser() 
  
  raw <- c(tcrossprod(d$yCenter)) 
  
  TT <- expand.grid(t1=d$t, t2=d$t) 
  
  cbind(TT, raw=raw) 
  
}) 



library(rgl) 

plot3d(rcov$t1, rcov$t2, rcov$raw) 



hCov <- 0.2 

resCov <- locfit(raw ~ lp(t1, t2, deg=1, h=hCov), rcov %>% filter(t1 != t2), ev=lfgrid(mg=ngrid), kern='epan') 

covMat <- matrix(predict(resCov), ngrid, ngrid) 

persp3d(tGrid, tGrid, covMat, add=FALSE, col='white') 



eig <- eigen(covMat) 

eig$values 

rmInd <- eig$values <= 0 

eig$values <- eig$values[!rmInd] 

eig$vectors <- eig$vectors[, !rmInd, drop=FALSE] 

lam <- eig$values * diff(range(tGrid)) / ngrid 

phi <- eig$vectors / sqrt(diff(range(tGrid)) / ngrid) 



matplot(tGrid, phi[, 1:3], type='l') 



# diagnonal elemenet 

diagRes <- locfit(raw ~ lp(t1, deg=1, h=hCov),  
                  
                  rcov %>% filter(t1 == t2),  
                  
                  ev=lfgrid(mg=ngrid)) 

Vhat <- predict(diagRes) 

lines3d(tGrid, tGrid, Vhat) 

sig2 <- mean(Vhat - diag(covMat)) 



# BLUP step 

K <- 3 

LambdaK <- diag(lam[seq_len(K)], nrow=K) 

xi <- plyr::ddply(dat, 'id', function(d) { 
  
  #browser() 
  
  str(d) 
  
  # Linear interpolation 
  
  Phii <- matrix(fdapace::ConvertSupport(tGrid, d$t, phi=phi[, seq_len(K), drop=FALSE]), ncol=K) 
  
  SigYi <- fdapace::ConvertSupport(tGrid, d$t, Cov=covMat) + diag(sig2, nrow=nrow(d)) 
  
  res <- c(LambdaK %*% t(Phii) %*% solve(SigYi, d$yCenter)) 
  
  names(res) <- paste0('xi', seq_len(K)) 
  
  res 
  
}) 



plot(xi$xi1, xi$xi2) 



# X_K 

XK <- as.matrix(xi[, -1]) %*% t(phi[, seq_len(K), drop=FALSE]) + matrix(muHat, nrow=nrow(xi), ncol=ngrid, byrow=TRUE) 

plotInd <- c(1:4, 35) 

matplot(tGrid, t(XK[plotInd, ]), type='l') 

lines(tGrid, muHat, lwd=3) 

points(dat$t[dat$id == 53], dat$y[dat$id == 53], cex=2, col='green') 

points(dat$t[dat$id == 127], dat$y[dat$id == 127], cex=2, col='cyan') 



plot(xi$xi1, xi$xi2) 

points(xi$xi1[plotInd], xi$xi2[plotInd], type='p', col=plotInd, cex=4) 





library(fdapace) 

# devtools::load_all('GoogleDrive/Research/RPACE/tPACE/') 



dat <- read.delim('http://www.statsci.org/data/oz/wallaby.txt') %>%  
  
  select(Anim, Age, Leng) %>%  # select takes the columns 
  
  mutate(Age = Age / 365.24, Leng = Leng / 10) %>%  
  
  na.omit %>% 
  
  filter(Age >= 1 & Age <= 1.8) %>%  
  
  dplyr::rename(t = Age, y = Leng, id = Anim) 



samp <- MakeFPCAInputs(dat$id, dat$t, dat$y) 

res <- FPCA(samp$Ly, samp$Lt) 

str(res) 

plot(res) 



CreatePathPlot(res) 

CreatePathPlot(res, subset=1:4, showMean = TRUE) 



CI <- fitted(res, ciOptns=list(alpha=0.05, cvgMethod='band')) 

matlines(CI$workGrid, CI$cvgUpper[2, ], col='red', lty=4) 

matlines(CI$workGrid, CI$cvgLower[2, ], col='red', lty=4) 



?FPCA 



# Nothing was wrong except for a glowing typo, which is corrected now. 

resTrunc <- FPCA(samp$Ly, samp$Lt, optns=list(outPercent=c(0.1, 1))) 

plot(resTrunc) 

CreatePathPlot(resTrunc, subset=1:5) 